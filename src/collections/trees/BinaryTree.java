package collections.trees;

import java.util.BitSet;

/**
 * A BinaryTree implementation for values that can be represented as BitSets.
 * Especially useful when creating trees of IP prefixes where each node or leaf contains a value.
 * @param <A> The type of the value to store at each node.
 */
public class BinaryTree<A>
{
    /** The root note of the BinaryTree. */
    private Node<A> root = new Node<>(null);

    /**
     * Retrieves the node that has the largest prefix match with set.
     * @param set The binary values against which a largest prefix should be matched.
     * @param prefix_length The maximum prefix_length that will be matched.
     * @return The node which has the largest prefix of set which is lower or equal to prefix_length.
     */
    public Node<A> get(BitSet set, int prefix_length)
    {
        Node<A> node = root;

        // Take into account that the bits in each byte starts in reverse order.
        for(int i = 0; i <= prefix_length / 8; i++)
            for(int j = 0; j < 8 && (i*8 + j) < prefix_length; j++)
            {
                if(set.get(i*8 + (7 - j)))
                {
                    if(node.one == null)
                        return node;
                    else
                        node = node.one;
                }
                else
                {
                    if(node.zero == null)
                        return node;
                    else
                        node = node.zero;
                }
        }

        return node;
    }

    /**
     * Retrieves the exact prefix of set or returns null otherwise.
     * @param set The binary values against which a prefix should be matched.
     * @param prefix_length The prefix_length that will be matched.
     * @return The Node which the exact same prefix as set with prefix_length or null otherwise.
     */
    public Node<A> getExact(BitSet set, int prefix_length)
    {
        Node<A> node = root;

        // Take into account that the bits in each byte starts in reverse order.
        for(int i = 0; i <= prefix_length / 8; i++)
            for(int j = 0; j < 8 && (i*8 + j) < prefix_length; j++)
            {
                if(set.get(i*8 + (7 - j)))
                {
                    if(node.one == null)
                        return null;
                    else
                        node = node.one;
                }
                else
                {
                    if(node.zero == null)
                        return null;
                    else
                        node = node.zero;
                }
            }

        return node;
    }

    /**
     * Inserts a new value into the BinaryTree
     * @param set The prefix that should be added.
     * @param prefix_length The length of the prefix to be added.
     * @param value The value to be added.
     * @return The Node that was inserted in the tree.
     */
    public Node<A> insert(BitSet set, int prefix_length, A value)
    {
        Node<A> node = root;

        // Take into account that the bits in each byte starts in reverse order.
        for(int i = 0; i <= prefix_length / 8; i++)
            for(int j = 0; j < 8 && (i*8 + j) < prefix_length; j++)
            {
                if(set.get(i*8 + (7 - j)))
                {
                    if(node.one == null)
                        node.one = new Node<>(node);

                    node = node.one;
                }
                else
                {
                    if(node.zero == null)
                        node.zero = new Node<>(node);

                    node = node.zero;
                }
            }

        node.value = value;
        return node;
    }
}
