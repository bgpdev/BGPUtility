package collections.trees;

/**
 * A node of the BinaryTree
 * @param <A> The type of value that this node can hold.
 */
public class Node<A>
{
    /** The parent node of this node, null if it is the root node. */
    public final Node<A> parent;

    /** The children associated with a value of zero or one. */
    public Node<A> one = null;
    public Node<A> zero = null;

    /** The value belonging to this node. */
    public A value = null;

    /**
     * Constructs a Node for the BinaryTree.
     * @param parent The parent node of this tree.
     */
    Node(Node<A> parent)
    {
        this.parent = parent;
    }

    /**
     * Retrieves the child node associated with bit.
     * @param bit Whether to retrieve the left (zero) or right (one) child.
     * @return The child associated with the bit, null if it has no child for the given value.
     */
    public Node<A> get(boolean bit)
    {
        if(bit)
            return one;
        else
            return zero;
    }
}