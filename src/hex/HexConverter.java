package hex;

/**
 * Converts a byte array to a hexadecimal string.
 */
public class HexConverter
{
    /** Static character array to enhance performance. */
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Converts a byte array to a hexadecimal String.
     * @param bytes The byte array to be converted.
     * @return The Hexadecimal String
     */
    public static String toHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
