package utility;

public class Pair<T, U>
{
    private final T first;
    private final U second;

    public Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }

    public T getFirst() { return first; }
    public U getSecond() { return second; }

    public boolean equals(Pair<T, U> pair)
    {
        return pair.getFirst().equals(first) &&
                pair.getSecond().equals(second);
    }

    public int hashCode()
    {
        return first.hashCode() + 64 * second.hashCode();
    }
}
