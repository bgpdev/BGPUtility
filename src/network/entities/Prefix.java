package network.entities;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.UnknownHostException;

/**
 * Represents an IPv4 or IPv6 prefix.
 */
public class Prefix implements Comparable<Prefix>
{
    /** The length of the prefix. */
    public transient final int length;

    /** The bytes of the prefix. */
    public transient final byte[] prefix;

    /** The type of prefix, either IPv4 or IPv6 */
    public transient final Type type;

    @Override
    public int compareTo(Prefix o)
    {
        // If prefixes are not equal
        if (length != o.length)
            return length > o.length ? 1 : -1;

        int i = 0;
        while (i < (length + 7) / 8)
        {
            if (prefix[i] > o.prefix[i])
                return 1;
            else if (prefix[i] < o.prefix[i])
                return -1;

            i++;
        }

        return 0;
    }

    /**
     * Enumerates the possible prefix types.
     * Currently only IPv4 or IPv6
     */
    public enum Type
    {
        IPv4,
        IPv6
    }

    /**
     * A Serialized class which can be used to serialize this prefix to JSON.
     */
    public static class Serializer extends TypeAdapter<Prefix>
    {
        @Override
        public void write(JsonWriter writer, Prefix prefix) throws IOException
        {
            writer.value(prefix.toString());
        }

        @Override
        public Prefix read(JsonReader reader) throws IOException
        {
            return null;
        }
    }

    /**
     * Constructs a prefix from the data stream.
     * The reading format is [length (byte)][prefix (smallest amount of bytes necessary]
     * @param stream The DataInputStream from which the prefix should be read.
     * @param type The type of prefix of the prefix (IPv4 or IPv6)
     * @throws IOException Any exceptions that occur while reading from the DataInputStream.
     */
    public Prefix(DataInputStream stream, Type type) throws IOException
    {
        this.type = type;

        length = stream.readUnsignedByte();
        if(length != 0)
        {
            prefix = new byte[(length + 7) / 8];
            stream.readFully(prefix);
        }
        else
            prefix = null;
    }

    /**
     * Prints out the prefix.
     */
    public void print()
    {
        System.out.println("Prefix::Type: " + type.toString());
        System.out.println("Prefix::Length: " + length);
        System.out.println("Prefix::Prefix: " + toString());
    }

    /**
     * Converts the prefix to string representation.
     * @return The prefix converted to string representation.
     */
    public String toString()
    {
        if(type == Type.IPv4)
            return toIPv4String() + "/" + length;
        else
            return toIPv6String() + "/" + length;
    }

    /**
     * Converts an IPv4 prefix to IPv4 string representation.
     * @return The IPv4 prefix converted to IPv4 string representation.
     */
    private String toIPv4String()
    {
        if(prefix == null)
            return "0.0.0.0";

        byte[] buffer = new byte[4];
        System.arraycopy(prefix, 0, buffer, 0, (length + 7) / 8);

        return Byte.toUnsignedInt(buffer[0]) + "." +
                Byte.toUnsignedInt(buffer[1]) + "." +
                Byte.toUnsignedInt(buffer[2]) + "." +
                Byte.toUnsignedInt(buffer[3]);
    }

    /**
     * Converts an IPv6 prefix to IPv6 string representation.
     * @return The IPv6 prefix converted to IPv6 string representation.
     */
    private String toIPv6String()
    {
        if(prefix == null)
            return "::";

        byte[] buffer = new byte[16];
        System.arraycopy(prefix, 0, buffer, 0, (length + 7) / 8);

        try
        {
            return Inet6Address.getByAddress(buffer).getHostAddress();
        }
        catch(UnknownHostException exception)
        {
            exception.printStackTrace();
            return null;
        }
    }

    /**
     * Checks whether the string provided is a valid prefix.
     * @param prefix The string that is being checked.
     * @return True if this string is valid prefix, false otherwise.
     */
    public static boolean isPrefix(String prefix)
    {
        String[] split = prefix.split("/");
        return split.length == 2 && IP.isValid(split[0]) && split[1].matches("[0-9]+");
    }
}
