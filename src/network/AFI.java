package network;

/**
 * Address Family Numbers as specified by IANA
 * Source: https://www.iana.org/assignments/address-family-numbers/address-family-numbers.xhtml
 */
public enum AFI
{
    RESERVED(0),
    IPV4(1),
    IPV6(2),
    NSAP(3),
    HDLC(4),
    BBN(5),
    W802(6),
    E163(7),
    E164(8),
    F69(9),
    X121(10),
    IPX(11),
    APPLETALK(12),
    DECNET_IV(13),
    BANYAN_VINES(14),
    E164_NSAP(15),
    DNS(16),
    DN(17),
    ASN(18),
    XTP_IPV4(19),
    XTP_IPV6(20),
    XTP(21),
    FCWWPN(22),
    FCWWNN(23),
    GWID(24),
    L2VPN(25),
    MPLS_TP_SEI(26),
    MPLS_TP_LSP(27),
    MPLS_TP_PEI(28),
    MT_IPV4(29),
    MT_IPV6(30),

    EIGRP_CSF(16384),
    EIGRP_IPV4(16385),
    EIGRP_IPv6(16386),
    LCAF(16387),
    BGP_LS(16388),
    MAC48(16389),
    MAC64(16390),
    OUI(16391),
    MAC24(16392),
    MAC40(16393),
    IPV4_64(16394),
    RBRIDGE(16395),
    TRILL(16396);


    /** The numeric value of the subtype. */
    public final int value;

    /**
     * Constructs a Address Family Identifier
     * @param value The numeric value of the AFI.
     */
    AFI(int value)
    {
        this.value = value;
    }

    /**
     * Constructs a Address Family Identifier from a numeric value.
     * @param value The numeric value that should be converted.
     * @return The enumeration value matching this numeric value.
     */
    public static AFI fromInt(int value) throws IllegalArgumentException
    {
        for (AFI afi : AFI.values())
            if (afi.value == value) return afi;
        throw new IllegalArgumentException("Illegal Address Family Identifier: " + value);
    }
}